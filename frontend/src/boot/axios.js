import { boot } from 'quasar/wrappers'
import axios from 'axios'
import { useAuthStore } from 'src/stores/auth.store'

// Be careful when using SSR for cross-request state pollution
// due to creating a Singleton instance here;
// If any client changes this (global) instance, it might be a
// good idea to move this instance creation inside of the
// "export default () => {}" function below (which runs individually
// for each client)
const api = axios.create({
  baseURL: process.env.DEV ? process.env.API_SERVER_URL_DEV : process.env.API_SERVER_URL_PROD
})

// Add a request interceptor
api.interceptors.request.use(
  async function (config) {
    // Do something before request is sent
    const authStore = useAuthStore()

    // Verifica quanto tempo falta para o token expirar
    // Se necessário atualiza o mesmo usando o refresh token (cookie)
    // console.log(Date.now(), authStore.decodedToken.exp * 1000, authStore.decodedToken.exp * 1000 - Date.now())
    if (authStore.decodedToken.exp * 1000 - Date.now() < 10000) { // 10000 = 10s
      authStore.refreshToken()
    }

    config.headers.Authorization = 'Bearer ' + authStore.accessToken
    return config
  },

  function (error) {
    // Do something with request error
    // notifyError(error.message)
    return Promise.reject(error)
  }
)

export default boot(({ app }) => {
  // for use inside Vue files (Options API) through this.$axios and this.$api

  app.config.globalProperties.$axios = axios
  // ^ ^ ^ this will allow you to use this.$axios (for Vue Options API form)
  //       so you won't necessarily have to import axios in each vue file

  app.config.globalProperties.$api = api
  // ^ ^ ^ this will allow you to use this.$api (for Vue Options API form)
  //       so you can easily perform requests against your app's API
})

export { api }
