import { defineStore } from 'pinia'
import { api } from 'src/boot/axios'
import axios from 'axios'

export const useAuthStore = defineStore('auth', {
  state: () => ({
    accessToken: '',
    loggedIn: false
  }),

  getters: {
    decodedToken (state) {
      if (state.accessToken.length > 0) {
        const base64Url = state.accessToken.split('.')[1]
        const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/')
        const jsonPayload = decodeURIComponent(window.atob(base64).split('').map(function (c) {
          return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
        }).join(''))
        return JSON.parse(jsonPayload)
      } else return ''
    }

  },

  actions: {
    async login () {
      const config = {
        withCredentials: true
      }
      const result = await api.get('/auth/login', config)
      // console.log('atLogin:\n', result)
      this.accessToken = result.data.accessToken
      this.loggedIn = true
    },

    async verifyLogin () {
      await api.get('/auth/verifylogin')
    },

    async refreshToken () {
      try {
        const result = await axios.get('http://localhost:3000/auth/refreshtoken', {
          withCredentials: true
        })
        this.accessToken = result.data.accessToken
        this.loggedIn = true
      } catch (error) {
        if (error.response) {
          if (error.response.data.code === 'FST_JWT_AUTHORIZATION_TOKEN_EXPIRED') {
            // Refresh token expirado, faz logout
            this.logout()
          }
        }
      }
    },

    async logout () {
      const config = {
        withCredentials: true
      }
      await api.get('/auth/logout', config)
      this.accessToken = ''
      this.loggedIn = false
    },

    isLogged () {
      return this.loggedIn
    }
  }
})
