'use strict'
const dayjs = require('dayjs')

module.exports = async function (fastify, opts) {
  fastify.get('/test', async function (request, reply) {
    return 'this is an example'
  })

  fastify.get('/login', async (request, reply) => {
    const accessToken = await createAccessToken()

    const refreshToken = await fastify.jwt.sign({
      userId: '',
      tenantId: '',
      name: 'bar'
    }, {
      expiresIn: process.env.JWT_REFRESH_TOKEN_EXPIRING_TIME_HOURS + 'h'
    })

    reply
      .setCookie('refreshToken', refreshToken, {
        domain: process.env.DOMAIN,
        path: '/',
        secure: true, // send cookie over HTTPS only
        httpOnly: true,
        sameSite: true, // alternative CSRF protection,
        signed: true,
        expires: dayjs().add(process.env.JWT_REFRESH_TOKEN_EXPIRING_TIME_HOURS, 'hour').toDate()
      })
      .code(200)
      .send({ accessToken })
  })

  fastify.get('/verifylogin', {
    onRequest: [fastify.authenticate]
  }, async (request, reply) => {
    await reply.send({ code: 'OK', message: 'it works!' })
  })

  fastify.get('/refreshtoken', {
    onRequest: [fastify.authenticateRefreshToken]
  }, async (request, reply) => {
    if (request.cookies?.refreshToken) {
      // Verifying refresh token
      request.jwtVerify({ onlyCookie: true })
      const accessToken = await createAccessToken()

      reply.code(200).send({ accessToken })
    } else {
      return reply.code(406).send()
    }
  })

  fastify.get('/logout', {
    // onRequest: [fastify.authenticate]
  }, async (request, reply) => {
    reply
      .clearCookie('refreshToken')
      .code(200)
      .send()
  })

  function createAccessToken () {
    return fastify.jwt.sign({
      name: 'foo',
      role: ['admin', 'spy']
    }, {
      expiresIn: process.env.JWT_ACCESS_TOKEN_EXPIRING_TIME_MINUTES + 'm'
    })
  }
}
