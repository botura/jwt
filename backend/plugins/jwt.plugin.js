'use strict'

const fp = require('fastify-plugin')
const jwt = require('@fastify/jwt')

/**
 * This plugins adds some utilities to handle http errors
 *
 * @see https://github.com/fastify/fastify-sensible
 */
module.exports = fp(async function (fastify, opts) {
  fastify.register(jwt, {
    secret: process.env.JWT_SECRET,
    cookie: {
      cookieName: 'refreshToken',
      signed: true
    }
  })

  fastify.decorate('authenticate', async function (request, reply) {
    try {
      await request.jwtVerify()
    } catch (err) {
      reply.code(401).send(err)
    }
  })

  fastify.decorate('authenticateRefreshToken', async function (request, reply) {
    try {
      await request.jwtVerify({ onlyCookie: true })
    } catch (err) {
      reply.code(401).clearCookie('refreshToken').send(err)
    }
  })

  fastify
    .register(require('@fastify/cookie'), {
      secret: process.env.COOKIE_SECRET // for cookies signature
    })
})
