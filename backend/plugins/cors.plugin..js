'use strict'

const fp = require('fastify-plugin')

module.exports = fp(async function (fastify, opts) {
  console.log(process.env.FRONTEND_URL)
  fastify.register(require('@fastify/cors'), {
    origin: [
      process.env.FRONTEND_URL
    ],
    credentials: true
  })
})
